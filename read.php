<?php

include "conn.php";

$sql = "SELECT id, nama, jawatan, nokp FROM profil";
$rsQuery = mysqli_query($conn,$sql);

if (mysqli_num_rows($rsQuery) > 0)
{
	$msg['status'] = 1;

	while($data = mysqli_fetch_assoc($rsQuery)) {
	
		$msg['profil'][] = array(
			"id" 	  => $data['id'] ,
			"nama" 	  => $data['nama'],
			"jawatan" => $data['jawatan'],
			"nokp" 	  => $data['nokp']
		);
	}
}else{
	$msg = array(
		"status" => 0,
		"msg" => "Tiada rekod ditemui"
	);
}

mysqli_close($conn);

header('Content-Type: text/javascript; charset=utf8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$callback = $_GET['callback'];
echo $callback.'('.json_encode($msg).');';
exit;

