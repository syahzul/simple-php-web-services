# Simple PHP Web Services

This is just a simple PHP codes that shows how to:

* Create new record
* Search single record
* Search multiple records
* Update record

This is a companion to [https://gitlab.com/syahzul/sample-mobile-app-using-web-services](Simple Cordova Mobile App)