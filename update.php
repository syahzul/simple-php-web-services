<?php

include "conn.php";

$id      = $_REQUEST['id'];
$nama    = $_REQUEST['nama'];
$jawatan = $_REQUEST['jawatan'];
$nokp    = $_REQUEST['nokp'];

$sql = "UPDATE profil SET nama = '{$nama}', jawatan = '{$jawatan}', nokp = '{$nokp}' WHERE id = '{$id}'";

if (mysqli_query($conn, $sql)) {
    $msg = array(
        'status' => 1,
        'msg' => 'Yay! Maklumat pegawai telah dikemaskini.'
    );
}
else {
    $msg = array(
        'status' => 0,
        'msg' => mysqli_error($conn)
    );
}

mysqli_close($conn);

header('Content-Type: text/javascript; charset=utf8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$callback = $_GET['callback'];
echo $callback.'('.json_encode($msg).');';
exit;