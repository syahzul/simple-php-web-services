/*
MySQL Data Transfer
Source Host: localhost
Source Database: json_crud
Target Host: localhost
Target Database: json_crud
Date: 27/08/2015 10:13:31 AM
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for profil
-- ----------------------------
DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama` varchar(80) DEFAULT NULL,
  `jawatan` varchar(80) DEFAULT NULL,
  `nokp` char(12) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records 
-- ----------------------------
INSERT INTO `profil` VALUES ('1', 'Subhee', 'PTM', '797979797979');
INSERT INTO `profil` VALUES ('2', 'Norhardi', 'PTM', '799999999999');
INSERT INTO `profil` VALUES ('3', 'Fairuz', 'PTM', '111111111111');
INSERT INTO `profil` VALUES ('4', 'Izdi', 'PTM', '222222222222');
INSERT INTO `profil` VALUES ('5', 'Norhayani', 'PTM', '333333333333');
INSERT INTO `profil` VALUES ('6', 'Ilyia', 'PTM', '444444444444');
INSERT INTO `profil` VALUES ('7', 'Lidya', 'PPTM', '555555555555');
INSERT INTO `profil` VALUES ('8', 'Nuridayu', 'PPTM', '666666666666');
INSERT INTO `profil` VALUES ('9', 'Mazuina', 'PPTM', '777777777777');
INSERT INTO `profil` VALUES ('10', 'Sarizal', 'PPTM', '888888888888');
INSERT INTO `profil` VALUES ('11', 'Yaseerah', 'PPTM', '999999999999');
INSERT INTO `profil` VALUES ('12', 'Norliza', 'PPTM', '123456789');
