<?php

include "conn.php";

$nama    = $_REQUEST['nama'];
$jawatan = $_REQUEST['jawatan'];
$nokp    = $_REQUEST['nokp'];

$sql = "INSERT INTO profil (nama, jawatan, nokp) VALUES('{$nama}', '{$jawatan}', '{$nokp}')";

if (mysqli_query($conn, $sql)) {
    $msg = array(
        'status' => 1
    );
}
else {
    $msg = array(
        'status' => 0,
        'msg' => mysqli_error($conn)
    );
}

mysqli_close($conn);

header('Content-Type: text/javascript; charset=utf8');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

$callback = $_GET['callback'];
echo $callback.'('.json_encode($msg).');';
exit;